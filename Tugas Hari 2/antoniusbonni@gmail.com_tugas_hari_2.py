print("Soal 1 Hello World")
print("Hello World Python\n")

print("Soal 2 Aritmatika")
a=1
b=2
print("a=1\nb=2")
print(f"a+b={a+b}\n")

# print("\nSoal 2 -")
a=5
b=2
print("a=5\nb=2")
print(f"a-b={a-b}\n")

# print("\nSoal 2 x")
a=5
b=2
print("a=5\nb=2")
print(f"axb={a*b}\n")

# print("\nSoal 2 x")
a=4
b=2
print("a=4\nb=2")
print(f"a/b={a//b}\n")

print("Soal 3: Assign Variable dan Tipe Data Integer, Float")
a = 20
b = 10

results_divided_int = a//b
results_divided_float = a/b
print(f"a:b")
print(f"{a}/{b}")
print(f"Integer -> {a}/{b} = {a//b}")
print(f"Float -> {a}/{b} = {a/b}")

print("\nSoal 4 String Operation")
firstname="antonius"
lastname="bonni"
print(f"Hello sanbercode, saya {firstname} {lastname}! saya siap belajar python backend development.")

print("\nSoal 5 Tipe Data")
usia = 33
kata = "usiaku"
print(kata+" "+str(usia)+" tahun")