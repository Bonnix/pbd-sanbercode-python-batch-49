#Soal 1: Perbedaan Data Structure
#List, Tuple, Set, dan Dictionary 

# # List
print("Soal 2: Akses List")
a = ['1', '13b', 'aa1', 1.32, 22.1, 2.34]
print(a[1:5])


print("\nSoal 3: Akses Nested List")
# Lengkap kode untuk menghasilkan suatu output yang di harapkan
a = [
    [5, 9, 8],
    [0, 0, 6]
    ]
print(a[1][1:3])


print("\nSoal 4: List Manipulation")
# Lengkapi kode untuk menghasilkan suatu output yang di harapkan
a = [
    [5, 9, 8],
    [0, 0, 6]
    ]
a[0][2]=10
a[1][0]=11
print(a)

print("\nSoal 5: Delete Element List")

# Lengkapi kode untuk menghasilkan suatu output yang di harapkan
areas = ["hallway", 11.25, "kitchen", 18.0,
        "chill zone", 20.0, "bedroom", 10.75,
         "bathroom", 10.50, "poolhouse", 24.5,
         "garage", 15.45]

# Hilangkan elemen yang bernilai "bathroom" dan 10.50 dalam satu statement code
del(areas[8])
print(areas)

print("\nSoal 6: List Comprehension")
# Gunakan metode **list comprehension** untuk mencari anggota dari S yang habis di bagi 2, kemudian assign hasilnya dalam bentuk list ke dalam variabel T.
S = [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
T = []

for angka in S:
    if angka%2==0:
      T.append(angka)
print(T)

print("\nSoal 7. Mengakses Tuple")
# Gunakan cara sclicing untuk mengakses tuple sehingga mendapatkan hasil sesuai expected output
tuple_1 = (1, 2, 6, 7, 8, 9, 10)
#akses tuple
# Expected Outpout:
# (6, 7, 8)
print(tuple_1[2:5])

print("\nSoal 8: Menambahkan key-value baru ke Dictionary")
# Lengkapi kode untuk menghasilkan suatu output yang di harapkan
europe = {'spain':'madrid', 'france':'paris', 'germany':'berlin', 'norway':'oslo' }
# tambahkan key itali ke objek dictionary dengan value roma
# cek apakah itali ada di dalam objek dictionary
# Expected Output:
# True
europe["italy"]="rome"
print(europe)
if "italy" in europe:
   print("True")

print("\nSoal 9: Update dan Remove Dictinary")
# Lengkapi kode untuk menghasilkan suatu output yang di harapkan
europe = {'spain':'madrid', 'france':'paris', 'germany':'bonn',
          'norway':'oslo', 'italy':'rome', 'poland':'warsaw',
          'australia':'vienna' }
# update nilai ibukota german ke berlin
europe['germany']='berlin'
# remove australia dari europa
del europe['australia']
print(europe)

print("\nSoal 10: Nested Dictionary")
# Lengkapi kode untuk menghasilkan suatu output yang di harapkan
country = { 
           'spain': { 'capital':'madrid', 'population':46.77 },
           'france': { 'capital':'paris', 'population':66.03 },
           'germany': { 'capital':'berlin', 'population':80.62 },
           'norway': { 'capital':'oslo', 'population':5.084 } 
         }
print(country['germany']['population'])

country['Indonesia']={'capital': 'jakarta', 'population': 250}
print(country)

print("\nSoal 11: Loop Dictionary:")
# Lengkapi kode untuk menghasilkan suatu output yang di harapkan
country = { 
           'spain': { 'capital':'madrid', 'population':46.77 },
           'france': { 'capital':'paris', 'population':66.03 },
           'germany': { 'capital':'berlin', 'population':80.62 },
           'norway': { 'capital':'oslo', 'population':5.084 },
           'indonesia' : {'capital':'jakarta', 'population':250}
         }

for ibukota,kota in country.items():
    print('Ibukota '+ibukota+' adalah ' + kota['capital'])

print("\nSoal 12: Remove Duplicate using set")
# Hilangkan nilai duplikat dari sebuah objek list dengan menggunakan cara set sehingga menjadi sebuah tipe set
obj_list = [1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 9, 10]
obj_list_conv = set(obj_list)
#using set
# Expected output: 
# {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
print(obj_list_conv)

print("\nSoal 13: Mengubah dan menghapus anggota set")
# Ubahlah dan hapus anggota set sehingga mendapatkan hasil yang diinginkan
set_1 = {1, 2, 3, 4, 5}
for angka in [6,7,8]:
  set_1.add(angka)
print(set_1)
#hapus nilai anggota set 8
set_1.discard(8)
print(set_1)
# Expected output:
# {1, 2, 3, 4, 5, 6, 7}

print("\nSoal 14: Operasi pada Set")
# Carilah irisan dari ke dua set dengan menggunakan metode intersection
set_2 = {6, 8, 9, 10, 24}
set_3 = {6, 10, 8, 25, 13}
#intersection set
# Expected output:
# {6, 8, 10}
print(sorted(set_2 & set_3))