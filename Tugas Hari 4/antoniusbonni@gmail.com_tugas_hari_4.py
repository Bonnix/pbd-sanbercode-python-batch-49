print("Soal 1: If-Else Statement")

# Lengkapi kode untuk menghasilkan suatu output yang di harapkan

# - Bualah sebuah if-else statement yang dimana akan mem-print 'High' jika grade adalah 'A' 
# dan price lebih dari 100000, kemudian mem-print 'Medium' jika grade adalah 'A' 
# dan price lebih dari 50000 dan memprint 'low' jika grade adalah 'A' dan price lebih kecil dan sama dengan 50000.
# '''

grade = 'A'
# price = int(input("Masukkan price: "))
price = 60000
if grade == 'A' and price > 100000:
  print("High")
elif grade == 'A' and price > 50000:
  print("Medium")
elif grade == 'A' and price <= 50000:
  print("Low")

print("\nSoal 2: For Loop")
# Lengkapi kode untuk menghasilkan suatu output yang diharapkan:
# *   Cari siswa mana saja yang memiliki nilai lebih dari sama dengan 80. Masukkan kedalam sebuah list. print hasilnya
# Expected output:
# ['Budi', 'Rudi', 'Leo']

# #kerjakan di bawah ini
data_siswa = [
    {
        "nama":"Budi",
        "nilai": 90
    },
    {
        "nama":"Nina",
        "nilai": 78
    },
    {
        "nama":"Rudi",
        "nilai": 91
    },
    {
        "nama":"Olivia",
        "nilai": 76
    },
    {
        "nama":"Leo",
        "nilai": 80
    },
    {
        "nama":"Liam",
        "nilai": 67
    },
    {
        "nama":"Sheila",
        "nilai": 76
    }
]

score_tinggi = []
# print(type(data_siswa))
# print(data_siswa[0]['nilai'])
for jml_siswa in data_siswa:
  if jml_siswa['nilai'] >= 80:
    score_tinggi.append(jml_siswa['nama'])

print(score_tinggi)
