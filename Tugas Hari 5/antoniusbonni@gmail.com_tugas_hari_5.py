print("Soal 1: Membuat Simple Function\n")
obj_list = [11.25, 18.0, 20.0, 10.75, 9.50, 13.45, 23.0, 7.0, 8.45, 2.50]
def mean_list(inp_list):
    # pass
    #lengkapi kode ini
    total_jumlah = sum(obj_list)
    # print(total_jumlah)
    count = len(obj_list)
    # print(count)
    if count > 0:
      results = total_jumlah/count
    return results

print(mean_list(obj_list))

print("\nSoal 2. Mapping Fungsi lambda")
obj_list = [i for i in range(1000)]

# angkagenap = 0
# for angka in obj_list:
#    if angka%2==0:
#     #  angkagenap.append(angka)
#     angkagenap += 1

# print(angkagenap)
# def angkagenap(obj_list):
#   angkagenap = 0
#   for angka in obj_list:
#     if angka%2==0:
#     #  angkagenap.append(angka)
#       angkagenap += 1
#     return angkagenap

# print(angkagenap)

# Menggunakan lambda function dengan fungsi map() dan filter()
even_numbers = filter(lambda x: x % 2 == 0, obj_list)
hasil = len(list(even_numbers))

print(hasil)