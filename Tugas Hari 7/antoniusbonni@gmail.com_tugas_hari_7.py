import json

# Soal no. 1
# read_txt(fname)

def read_txt(fname):
  file = open(fname)
  data = file.read()
  print(data)

# def main():
namafile = '1.txt'
read_txt(namafile)

# Soal no. 2
# cases = read_covid()
# print(cases)
def read_json(fname):
  file = open(fname)
  data = json.load(file)
  for isidata in data:
    print(isidata['Cases'], end=',')
#   print(data['Cases'])

namafile='2.json'
read_json(namafile)